var express = require('express');
var cors = require('cors')
var app = express();
app.use(cors());

var server = app.listen(8081, function () {
    console.log("Server runnning!");
})

app.get('/', function (req, res) {
    res.send('Hello World');
})

app.get('/alumnos', function (req, res) {
    res.send({
        alumnos: [
            'Julian',
            'Ramiro',
            'Leandro',
            'Tomas',
            'Maxi',
        ]
    });
})