import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';

import AlumnosOffline from './components/alumnos-offline/App';
import AlumnosOnline from './components/alumnos-online/App';
import Timer from './components/timer/App';

ReactDOM.render(<Timer />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
