import React from 'react';

class Listado extends React.Component {
  render() {
    return (
      <div>
        <ol>
          {this.props.alumnos.map(alumno => (
            <li>{alumno}</li>
          ))}
        </ol>
      </div>
    );
  }
}


export default Listado;