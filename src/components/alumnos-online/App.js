import React from 'react';
import '../../App.css';
import Formulario from './Formulario';
import Listado from './Listado';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      alumnos: []
    };
    this.agregarAlumno = this.agregarAlumno.bind(this);
  }

  componentDidMount() {
    fetch('http://localhost:8081/alumnos')
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.setState({
          alumnos: data.alumnos
        })
      });
  }

  render() {
    return (
      <div className="App">
        <h2>Alumnos Presentes: </h2>
        <Formulario agregarAlumno={this.agregarAlumno} />
        <Listado alumnos={this.state.alumnos} />
      </div>
    );
  }

  agregarAlumno(alumno) {
    this.setState(stateActual => ({
      alumnos: stateActual.alumnos.concat(alumno)
    }))
  }
}

export default App;
