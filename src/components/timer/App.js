import React from 'react';
import '../../App.css';
import Timer from './Timer';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Hola UNAHUR!</h1>
        <Timer texto="Segundos"/>
      </header>
    </div>
  );
}

export default App;
