import React from 'react';

class Formulario extends React.Component {
  constructor(props) {
    super(props);

    this.state = { 
      nombre: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input
          onChange={this.handleChange}
          value={this.state.nombre}
        />
        <button> Agregar Alumno </button>
      </form>
    );
  }

  handleChange(e) {
    this.setState({ nombre: e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();

    if (!this.state.nombre.length) {
      return;
    }

    this.props.agregarAlumno(this.state.nombre);

    this.setState({
      nombre: ''
    });
  }
}

export default Formulario;