# React Demo

Para correr la aplicacion, instalarse la utilidad [npm](hhttps://www.npmjs.com/get-npm).  
Luego ejecutar los siguientes comandos en el directorio principal:

### `npm run react`

Levanta el sitio web en [http://localhost:8080](http://localhost:8080)

### `npm run server`

Levanta el servidor en [http://localhost:8081](http://localhost:8081)

## Probar los distintos componentes

Por defecto, la demo comienza con el componente `Timer`. Para probar los distintos componentes, ingresar su nombre en el archivo [index.js](https://bitbucket.org/gm2dev/react-demo/src/1470cce941850b36c42f71bb59ccb43f27e2cfee/src/index.js#lines-10).  
Por ejemplo, si quiero probar la demo de Alumnos Online, reemplazar `Timer` por `AlumnosOnline` en dicho archivo.